/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysmobot;

import com.gargoylesoftware.htmlunit.html.DomElement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SysMo
 */
public abstract class MainBotGameDebateXPU extends Bot{

    public MainBotGameDebateXPU(String strRootPage) {
        super(strRootPage);
        // I only want the HTML's text!!!
        this._oWebClient.getOptions().setCssEnabled(false);
        this._oWebClient.getOptions().setDownloadImages(false);
        this._oWebClient.getOptions().setJavaScriptEnabled(false);    
        this._oWebClient.getOptions().setThrowExceptionOnScriptError(false);
        this._oWebClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
    }

    @Override
    protected ArrayList<HashMap> getDataAsADictionary() {
        // Array list with all wrappers that contain gpu's families
        ArrayList<DomElement> arrlDomElements = this.getDataElementByClass("darkBox2013 componentListTable", true);
        // Array with all families
        ArrayList<HashMap> arrlHashMapsFamilies = new ArrayList<HashMap>();
        // This HashMap will take the value of each item into the following loop
        HashMap hsmUnit = new HashMap();
        // The same with this DomElement
        DomElement oCurrentDomElement = null;
        // Loop over the Array of Wrappers
        for(Iterator<DomElement> item = arrlDomElements.iterator(); item.hasNext();){
            // Save the DomElement into a var
            oCurrentDomElement = item.next();
            // Get the Owner's String
            String strOwner = this.getOwner(oCurrentDomElement);
            // Init the Devices PCI's Bot
            Bot botDevicesPCI = new MainBotDevicesPCI("http://pci-ids.ucw.cz/read/PC");
            // Try to set the page
            try {
                botDevicesPCI.setSectionPage(strOwner);
            } catch (IOException ex) {
                Logger.getLogger(MainBotGameDebateXPU.class.getName()).log(Level.SEVERE, null, ex);
            }
            // Get all families
            ArrayList<DomElement> arrlSubDomElements = this.getDataElementByClassFromNode("hardwareRow", oCurrentDomElement, true);
            // Loop over each families
            for(Iterator<DomElement> subItem = arrlSubDomElements.iterator(); subItem.hasNext();){
                // Extract all Data
                hsmUnit = extractMainData(subItem.next());
                // Add to the HashMap the owner
                hsmUnit.put("Owner", strOwner);
                // Put the Bot Objet. It will use then
                hsmUnit.put("BotPCI", botDevicesPCI);
                // And finally add to the response array
                arrlHashMapsFamilies.add(hsmUnit);
            }
        }   
        // Return the HashMapFamilies for my children to use it 
        return arrlHashMapsFamilies;
    }

    @Override
    protected HashMap extractMainData(DomElement oDomElement) {        
        // Create a new HashMap
        HashMap hmpDictionaryPage = new HashMap();
        // Set all keys and values that I need
        hmpDictionaryPage.put("Family", this.getFamily(oDomElement));
        hmpDictionaryPage.put("FamilyURL", this.getFamilyURL(oDomElement));
        // Return this dictionary page        
        return hmpDictionaryPage;         
    }
    // Get the XPU's Owner (Nvidia, AMD, Intel, ETC)
    private String getOwner(DomElement oDomElement){        
        // I nkow that there is only one class with that name
        // but the getDataElementByClassFromNode returns me an array
        String strName = new String("");
        try{
            // Get the first element that contains the Owner
            DomElement oOwnerFullClassElement = this.getDataElementByClassFromNode("hardwareRowHead clearfix", oDomElement, false).iterator().next(); 
            // And into the element select another node with the Owner
            strName = this.getDataElementByClassFromNode("hardwareTitles", oOwnerFullClassElement, true).iterator().next().asText();
        } catch(Exception e){
            
        }     
        return strName;
    }
    // Get XPU's family, in the case of Nvidia (Serie 100, Serie 200, Titan, Tesla, etc)
    private String getFamily(DomElement oDomElement){
        // I nkow that there is only one class with that name
        // but the getDataElementByClassFromNode returns me an array
        String strName = new String("");
        try{
            // Get the first element that contains the Family Name
            DomElement oFamilyNameClassElement = this.getDataElementByClassFromNode("hardwareModel", oDomElement, false).iterator().next(); 
            // And set the element as a text
            strName = oFamilyNameClassElement.asText();
        } catch(Exception e){
            
        }     
        return strName;
    }
    // Get Family's URL
    private String getFamilyURL(DomElement oDomElement){
        // I nkow that there is only one class with that name
        // but the getDataElementByClassFromNode returns me an array
        String strName = new String("");
        try{
            // Get the first element that contains the Family's URL
            DomElement oFamilyURLClassElement = this.getDataElementByClassFromNode("hardwareModel", oDomElement, false).iterator().next(); 
            // And set the Element as a text
            strName = oFamilyURLClassElement.getElementsByTagName("a").iterator().next().getAttribute("href");
            strName = strName.replace("..", "");
        } catch(Exception e){
            
        }     
        return strName;
    }    
}