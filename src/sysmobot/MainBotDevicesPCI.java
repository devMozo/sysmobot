/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysmobot;

import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author SysMo
 */
public class MainBotDevicesPCI extends Bot{
    // HashMap with the availables owners
    private HashMap _hsmpOwnerAvailables;
    private String _strOwnerHexa;
    private String _strGameDebateDevices;

    public MainBotDevicesPCI(String strRootPage) {
        super(strRootPage);
        // I only want the HTML's text!!!
        this._oWebClient.getOptions().setCssEnabled(false);
        this._oWebClient.getOptions().setDownloadImages(false);
        this._oWebClient.getOptions().setJavaScriptEnabled(false);    
        this._oWebClient.getOptions().setThrowExceptionOnScriptError(false);
        this._oWebClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        this.initHsmpOwnerAvailables();
    }
    // Init the Owner ArrayList <-> PCI-ID
    private void initHsmpOwnerAvailables(){
        this._hsmpOwnerAvailables = new HashMap();
        // These are our available owners
        // This methods is a little "pedorro"
        this._hsmpOwnerAvailables.put("AMD", "/1002");
        this._hsmpOwnerAvailables.put("INTEL", "/8086");
        this._hsmpOwnerAvailables.put("NVIDIA", "/10de");        
    }
    // Get the Owner ID
    public String getStrOwnerHexa() {
        return _strOwnerHexa;
    }
    // Set the Game Debate's Device
    public void setStrGameDebateDevices(String strGameDebateDevices) {
        this._strGameDebateDevices = strGameDebateDevices;
    }
    
    @Override
    public void setSectionPage(String strOwner) throws IOException{
        // To loop this HashMap I need to convert it to a Map Object.
        Map<String, String> oMap = this._hsmpOwnerAvailables;
        // Loop the HashMap
        for (Map.Entry<String, String> entry : oMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            // If the Owner's String contains the key (All in lowercase mode)
            // I'm gonna get the value.
            if(strOwner.toLowerCase().contains(key.toLowerCase()))
                strOwner = value;
        }
        this._strGameDebateDevices = strOwner.replace("/", "");
        // Call the super method with the new URL
        super.setSectionPage(strOwner);
    }
    // Get the main <table/> tags that contain all rows with its PCI-IDs
    private DomElement getMyItemPCI(){
        // Get all <table/> tags
        DomNodeList<DomElement> dmnDomElements = this._oVictimWeb.getElementsByTagName("table");
        DomElement oElementTable = null,
                   oElementItem = null;
        ArrayList<DomElement> arrlElementsPCIIDs = new ArrayList<DomElement>();
        // Get the table element
        for(int i = 0; i < dmnDomElements.size(); i++)
            if(dmnDomElements.get(i).getAttribute("class").contains("subnodes"))
                oElementTable = dmnDomElements.get(i);
       // If it's not null
        if(oElementTable != null){
            // Get all items
            arrlElementsPCIIDs = this.getDataElementByClassFromNode("item", oElementTable, true);
            String strActualItem = new String();
            float iLastCoincidences = 0,
                  iNewCoincidence = 0;
            // Loop al items
            for(int i = 0; i < arrlElementsPCIIDs.size(); i++){
                // Get the text of the item
                // This item has 3 position
                // [0] => PCI-ID
                // [1] => Name  <--- This is that I want
                // [2] => Notes
                strActualItem = arrlElementsPCIIDs.get(i).getElementsByTagName("td").get(1).asText();
                // Get the coincidences between the 
                iNewCoincidence = this.getCoincidenceAverage(strActualItem,this._strGameDebateDevices);
                // If there are more coincidences than the previous 
                if(iNewCoincidence > iLastCoincidences){
                    // Set the new DomElement
                    oElementItem = arrlElementsPCIIDs.get(i);
                    // And update the Coincidences
                    iLastCoincidences = iNewCoincidence;
                }
            }
        }
        // Return the element
        return oElementItem;
    }    
    // Get coincidences average between two Strings
    private float getCoincidenceAverage(String strActualItem, String strToCompare){
        // Remove from both string the characters like "/", "[", "]", "(", ")"
        strActualItem = strActualItem.replaceAll("[/\\[\\]()-]", " ");
        strToCompare = strToCompare.replaceAll("[/\\[\\]()-]", " ");
        // Split string by white-space and save it into a String's Array
        String[] arrStrActualItem = strActualItem.split(" "),
                 arrStrToCompare = strToCompare.split(" ");
        // Unit words
        String strSanitizeActualItem = new String(),
               strSanitizeToCompare = new String(),
               strOnlyNumberActualItem = new String(),
               strOnlyNumberToCompare = new String();
        // Float elements
        float fCoincidences = 0,
              // Initially I put the division with the Actual Item String's length 
              fDivision = arrStrToCompare.length;
        // but if it lesser than the To Compare String' length
        /*if(fDivision < arrStrToCompare.length)
            fDivision = arrStrToCompare.length;*/
        // Loop both array and compare
        for (int i = 0; i < arrStrActualItem.length; i++){
            // Sanitize word
            strSanitizeActualItem = arrStrActualItem[i].toLowerCase().trim();
            strOnlyNumberActualItem = strSanitizeActualItem.replaceAll("[^0-9]", "");
            for (int j = 0; j < arrStrToCompare.length; j++){
                // Sanitize word
                strSanitizeToCompare = arrStrToCompare[j].toLowerCase().trim();
                strOnlyNumberToCompare = strSanitizeToCompare.replaceAll("[^0-9]", "");
                // Compare if the strings are equals or if cotains numbers, and that numbers sequence are the same.
                if(strSanitizeActualItem.equals(strSanitizeToCompare) 
                        || (!strOnlyNumberActualItem.isEmpty() && !strOnlyNumberToCompare.isEmpty() && strOnlyNumberActualItem.equals(strOnlyNumberToCompare))){
                    // One more to the counter
                    fCoincidences++;
                }
            }
        }
        // Division
        fDivision = fCoincidences/fDivision;
        // Return coincidences number
        return fDivision;
    }
    
    @Override
    protected ArrayList<HashMap> getDataAsADictionary() {
        // New dictionary
        ArrayList<HashMap> arrlMyDictionary = new ArrayList<HashMap>();
        // Get my item PCI
        DomElement oMyElement = this.getMyItemPCI();
        // Add to the dictionary the PCI-ID
        arrlMyDictionary.add(this.extractMainData(oMyElement));
        // Return Dictionary
        return arrlMyDictionary;
    }
    
    protected String getDataAsString() {
        // Get my item PCI
        DomElement oMyElement = this.getMyItemPCI();
        // Return String
        return (String)this.extractMainData(oMyElement).get("PCI-ID");
    }

    @Override
    protected HashMap extractMainData(DomElement oDomElement) {
        // Create a new HashMap
        HashMap hsmMap = new HashMap();
        // Set the "PCI-ID" value
        hsmMap.put("PCI-ID", this.getPCIID(oDomElement));
        // Return HashMap
        return hsmMap;
    }
    
    private String getPCIID(DomElement oDomElement){
        // By default the string is empty
        String strID = new String("");
        // Try to get the first <td> tags that contains the id
        try{   
           strID = oDomElement.getElementsByTagName("td").get(0).asText();
        } catch(Exception e){
            
        }
        // Return ID
        return strID;
    }
    
}
