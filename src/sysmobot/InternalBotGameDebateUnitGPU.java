/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysmobot;

import com.gargoylesoftware.htmlunit.html.DomElement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author SysMo
 */
public class InternalBotGameDebateUnitGPU extends InternalBotGameDebateUnitXPU{

    public InternalBotGameDebateUnitGPU(String strRootPage) {
        super(strRootPage);
    }
    
    @Override
    protected HashMap extractMainData(DomElement oDomElement) {    
        // Create a new HashMap
        HashMap hmpDictionaryPage = new HashMap();
        // Set all keys and values that I need       
        hmpDictionaryPage.put("GPUFeatures", this.getGPUFeatures(oDomElement));    
        hmpDictionaryPage.put("Compatibility", this.getCompatibility(oDomElement));
        hmpDictionaryPage.put("Memory", this.getMemory(oDomElement));
        hmpDictionaryPage.put("DisplayConnectors", this.getDisplayConnectors(oDomElement));
        hmpDictionaryPage.put("ClockSpeeds", this.getClockSpeeds(oDomElement));
        hmpDictionaryPage.put("Power", this.getPower(oDomElement));
        hmpDictionaryPage.put("RecommendedHardware", this.getRecommendedHardware(oDomElement));           
        hmpDictionaryPage.put("Image", this.getMainImageURL());
        // Return this dictionary page        
        return hmpDictionaryPage; 
    }
    // Get all GPU Features
    private HashMap getGPUFeatures(DomElement oDomElement){
        HashMap hsmData = new HashMap();
        
        try{
            DomElement oMySection = this.getMyElement(oDomElement,"GPU");
            hsmData = this.getHashMapFromTableElement(oMySection);
        } catch(Exception e){
        }
        // Return HashMap        
        return hsmData;
    }
    // Get All Compatability
    private HashMap getCompatibility(DomElement oDomElement){        
        HashMap hsmData = null;
        
        try{
            DomElement oMySection = this.getMyElement(oDomElement,"Compatibility");
            hsmData = this.getHashMapFromTableElement(oMySection);
        } catch(Exception e){            
        }  
        // Return HashMap        
        return hsmData;    
    }
    // Get Memory
    private HashMap getMemory(DomElement oDomElement){
        HashMap hsmData = new HashMap();
        
        try{
            DomElement oMySection = this.getMyElement(oDomElement,"Memory");
            hsmData = this.getHashMapFromTableElement(oMySection);
        } catch(Exception e){            
        }
        // Return HashMap        
        return hsmData;      
    }
    // Get all display Connectors
    private HashMap getDisplayConnectors(DomElement oDomElement){
        HashMap hsmData = new HashMap();
        
        try{
            DomElement oMySection = this.getMyElement(oDomElement,"Connectors");
            hsmData = this.getHashMapFromTableElement(oMySection);
        } catch(Exception e){            
        }
        // Return HashMap        
        return hsmData;      
    }
    // Get Clocks Speeds
    private HashMap getClockSpeeds(DomElement oDomElement){
        HashMap hsmData = new HashMap();
        
        try{
            DomElement oMySection = this.getMyElement(oDomElement,"Clock Speeds");
            hsmData = this.getHashMapFromTableElement(oMySection);
        } catch(Exception e){            
        }
        // Return HashMap        
        return hsmData;      
    }
    // Get Power
    private HashMap getPower(DomElement oDomElement){
        HashMap hsmData = new HashMap();
        
        try{
            DomElement oMySection = this.getMyElement(oDomElement,"Power");
            hsmData = this.getHashMapFromTableElement(oMySection);
        } catch(Exception e){            
        }
        // Return HashMap        
        return hsmData;      
    }
    // Get Recommended Hardware
    private HashMap getRecommendedHardware(DomElement oDomElement){
        HashMap hsmData = new HashMap();
        
        try{
            DomElement oMySection = this.getMyElement(oDomElement,"Recommended Hardware");
            hsmData = this.getHashMapFromTableElement(oMySection);
            hsmData.put("BestCPUMatch", this.getRecommendedMatchCPUID(oMySection));
            hsmData.put("GPUUpgrade", this.getRecommendedMatchGPUID(oMySection));
        } catch(Exception e){
        }
        // Return HashMap
        return hsmData;      
    }   
    // Get recommended CPU ID
    private String getRecommendedMatchCPUID(DomElement oDomElement){
        DomElement oCPUMatched = null;
        String strCPUMatched = new String("");
        // Try to get the main Element and get the HREF Attribute
        try{
            oCPUMatched = this.getHashMapFromTableElement(oDomElement, "BestCPUMatch");
            strCPUMatched = oCPUMatched.getElementsByTagName("a").iterator().next().getAttribute("href");
        } catch(Exception e){
        }
        finally{
            // Parse the string and get the ID
            strCPUMatched = GameDebateParser.parseAndReturnIDProcessor(strCPUMatched);
        }
        // Return ID
        return strCPUMatched;
    }
    // Get recommendned GPU ID
    private String getRecommendedMatchGPUID(DomElement oDomElement){
        DomElement oGPUMatched = null;
        String strGPUMatched = new String("");
        // Try to get the main Element and get the HREF Attribute
        try{
            oGPUMatched = this.getHashMapFromTableElement(oDomElement, "GPUUpgrade");
            strGPUMatched = oGPUMatched.getElementsByTagName("a").iterator().next().getAttribute("href");
        } catch(Exception e){
        }
        finally{
            // Parse the string and get the ID
            strGPUMatched = GameDebateParser.parseAndReturnIDGraphicCard(strGPUMatched);
        }
        // Return ID
        return strGPUMatched;
    }    
    // Get my section element by the category name (GPU, Compatibility, etc)
    protected DomElement getMyElement(DomElement oDomElement, String strKeyWord){
        ArrayList<DomElement> arrlDomElements = this.getDataElementByClassFromNode("hardware-info-box-category", oDomElement, true);
        DomElement oUnitElement = null;
        DomElement oMyElement = null;
        // Loop over the array
        for(Iterator<DomElement> item = arrlDomElements.iterator(); item.hasNext();){
            oUnitElement = item.next();
            // If this element contains the keyWord
            if(oUnitElement.asText().contains(strKeyWord)){
                // Get the parent 
                oMyElement = (DomElement)oUnitElement.getParentNode();
            }
        }
        // Returns the correct element
        return oMyElement;
    }
    // Get a HashMap from a DomElement TD Table
    protected HashMap getHashMapFromTableElement(DomElement oDomElement){
        // Array Element wit all fields
        ArrayList<DomElement> arrlDomElements = this.getDataElementByClassFromNode("hardware-info-row", oDomElement, true);
        // Unit element
        DomElement oUnitElement = null;
        // HashMap response
        HashMap hsmResponse = new HashMap();
        // Loop over all fields
        for(Iterator<DomElement> item = arrlDomElements.iterator(); item.hasNext();){
            oUnitElement = item.next();
            // Get the title(key), and the value(value)            
            DomElement oInfoTitle = this.getDataElementByClassFromNode("hardware-info-title", oUnitElement, true).iterator().next(),
                       oInfoValue = this.getDataElementByClassFromNode("hardware-info-value", oUnitElement, true).iterator().next(); 
            // Sanitize the data and put into the HashMap
            hsmResponse.put(this.processInfoTitle(oInfoTitle), this.processInfoValue(oInfoValue));
        }
        return hsmResponse;
    }
    // Get a HashMap from a DomElement TD Table
    protected DomElement getHashMapFromTableElement(DomElement oDomElement, String strKeyWord){
        // Array Element wit all fields
        ArrayList<DomElement> arrlDomElements = this.getDataElementByClassFromNode("hardware-info-row", oDomElement, true);
        // Unit element
        DomElement oUnitElement = null;
        // HashMap response
        DomElement oUnitSelected = null;
        // Loop over all fields
        for(Iterator<DomElement> item = arrlDomElements.iterator(); item.hasNext();){
            oUnitElement = item.next();
            // Get the title(key), and the value(value)            
            DomElement oInfoTitle = this.getDataElementByClassFromNode("hardware-info-title", oUnitElement, true).iterator().next();
            // Sanitize the data and put into the HashMap
            if(this.processInfoTitle(oInfoTitle).equals(strKeyWord)){
                oUnitSelected = oUnitElement;
            }            
        }
        return oUnitSelected;
    }
    
}