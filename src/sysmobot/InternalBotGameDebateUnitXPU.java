/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysmobot;

import com.gargoylesoftware.htmlunit.html.DomElement;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author user
 */
public abstract class InternalBotGameDebateUnitXPU extends Bot{
    
    public InternalBotGameDebateUnitXPU(String strRootPage) {
        super(strRootPage);
        // I only want the HTML's text!!!
        this._oWebClient.getOptions().setCssEnabled(false);
        this._oWebClient.getOptions().setDownloadImages(false);
        this._oWebClient.getOptions().setJavaScriptEnabled(false);    
        this._oWebClient.getOptions().setThrowExceptionOnScriptError(false);
        this._oWebClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
    }

    @Override
    protected ArrayList<HashMap> getDataAsADictionary() {
        // Get the wrapper by ID 
        DomElement oDomElement = this.getDataElementById("systemRequirementsOuterBox", true).iterator().next();
        // Create the dictionary
        ArrayList<HashMap> arrlDictionary = new ArrayList<HashMap>();
        // Add the features
        arrlDictionary.add(this.extractMainData(oDomElement));
        // Returnnnnn        
        return arrlDictionary;
    }
    @Override
    protected abstract HashMap extractMainData(DomElement oDomElement);// Get my section element by the category name (GPU, Compatibility, etc)
    // Get the main Image
    protected String getMainImageURL(){
        // Create a new String, by default is empty
        String strImage = new String("");
        // Wrapper Image
        ArrayList<DomElement> arrlWrapperImageElements = this.getDataElementByClass("gamPicBig", false);
        DomElement oUnit = null;
        // If there are a lot of wrapper, I find the correct checking the nameclass
        for(int i = 0; i<arrlWrapperImageElements.size(); i++){
            // Get <img/>
            oUnit = arrlWrapperImageElements.get(i).getElementsByTagName("img").iterator().next();
            // Check Class
            if(oUnit.getAttribute("class").contains("gamepicimg"))
                strImage = oUnit.getAttribute("src");
        }
        // Return this URL
        return strImage;
    }
    // Get my element by child and a kewword
    protected abstract DomElement getMyElement(DomElement oDomElement, String strKeyWord);
    // Get a HashMap from a DomElement TD Table
    protected abstract HashMap getHashMapFromTableElement(DomElement oDomElement);    
    // Process Title Info
    protected String processInfoTitle(DomElement oDomElement){
        // Create a new String
        String strInfoTitle = oDomElement.asText();
        // Remove end lines, whites space, etc
        strInfoTitle = strInfoTitle.replace("\n", "").replace("\r", "").replace(" ", "");
        // If there is a "(" character, I parse the String and remove all text following
        if(strInfoTitle.contains("(")){
            Integer iFirstOcurrence = strInfoTitle.indexOf("(");
            strInfoTitle = strInfoTitle.substring(0, iFirstOcurrence);
        }        
        // Return String
        return strInfoTitle;
    }
    // Process Value Info
    protected String processInfoValue(DomElement oDomElement){
        // Create a new String
        String strInfoValue = new String("");
        // If the value has img elements
        if(oDomElement.getElementsByTagName("img").iterator().hasNext()){
            // And the oDomElement has a sub element with the class called "driverSupportScoreWrapper"
            if(this.getDataElementByClassFromNode("driverSupportScoreWrapper", oDomElement, true).iterator().hasNext()){
                // Get the number of yellow stars (ranking)
                Integer numYellowStars = new Integer(this.getDataElementByAttributeFromNode("alt", "golden star", oDomElement, true).size());
                // Convert to String
                strInfoValue = numYellowStars.toString();
            }
            else{
                // Get the source of the image, it can be a "tick.png" or "cross.png". It mean "Yes" or "No"
                String sourceImage = oDomElement.getElementsByTagName("img").iterator().next().getAttribute("src");
                
                // Check the content of the source and set the correct String
                if(sourceImage.contains("tick"))
                    strInfoValue = "Yes";
                else
                    if(sourceImage.contains("cross"))
                        strInfoValue = "No";
            }
        }
        else{
            // If not, set as text :D
            strInfoValue = oDomElement.asText();
        }
        // return the String
        return strInfoValue;
    }    
}
