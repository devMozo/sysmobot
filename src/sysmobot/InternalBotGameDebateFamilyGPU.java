/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysmobot;

import com.gargoylesoftware.htmlunit.html.DomElement;
import java.util.HashMap;

/**
 *
 * @author user
 */
public class InternalBotGameDebateFamilyGPU extends InternalBotGameDebateFamilyXPU{
    
    public InternalBotGameDebateFamilyGPU(String strRootPage) {
        super(strRootPage);
    }     
    @Override
    protected String getIDXPU(DomElement oDomElement){
        // I nkow that there is only one class with that name
        // but the getDataElementByClassFromNode returns me an array
        String strID = new String("");
        try{
            // Get the URL
            strID = this.getURLXPU(oDomElement);
            // Parser and get the ID
            strID = GameDebateParser.parseAndReturnIDGraphicCard(strID);
        } catch(Exception e){
            
        }     
        return strID;        
    }   
    // Get Perfomance GPU
    protected HashMap getPerformanceXPU(String strURL){
        // Create a HashMap
        HashMap hsmpPerformance = new HashMap();
        // New Bot that wilprotectedl analize all GPU's Page Section
        Bot botForEachGPU = new InternalBotGameDebateUnitGPU(this.getStrRootPage());
        // Try to get the data from that gpu's page
        try{
            botForEachGPU.setSectionPage(strURL);
            hsmpPerformance = botForEachGPU.getDataAsADictionary().iterator().next();
        }
        catch(Exception e){}
        // Return HashMap
        return hsmpPerformance;
    }    
}
