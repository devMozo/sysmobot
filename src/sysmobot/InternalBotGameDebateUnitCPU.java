/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysmobot;

import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author SysMo
 */
public class InternalBotGameDebateUnitCPU extends InternalBotGameDebateUnitXPU{

    public InternalBotGameDebateUnitCPU(String strRootPage) {
        super(strRootPage);
    }

    @Override
    protected HashMap extractMainData(DomElement oDomElement) {
        // Create a new HashMap
        HashMap hmpDictionaryPage = new HashMap();
        // Set all keys and values that I need       
        hmpDictionaryPage.put("Details", this.getDetailsCPU(oDomElement));    
        hmpDictionaryPage.put("Features", this.getFeaturesCPU(oDomElement));
        hmpDictionaryPage.put("Image", this.getMainImageURL());
        // Return this dictionary page        
        return hmpDictionaryPage; 
    }
    // Get all details about the CPU
    private HashMap getDetailsCPU(DomElement oDomElement) {
        HashMap hsmResponse = new HashMap();
        // Try to get the main element and get the table's data        
        try{
            DomElement oMyElement = this.getMyElement(oDomElement, "Details");
            hsmResponse = this.getHashMapFromTableElement(oMyElement);            
            // However I need to get manually the MainLink
            hsmResponse.put("MainLink", this.getMainLink(oMyElement));
        } catch(Exception e){
        }        
        // Return the hashmap
        return hsmResponse;
    }
    // Get all features about the CPU
    private HashMap getFeaturesCPU(DomElement oDomElement) {        
        HashMap hsmResponse = new HashMap();
        // Try to get the main element and get the table's data
        try{
            DomElement oMyElement = this.getMyElement(oDomElement, "Features");
            hsmResponse = this.getHashMapFromTableElement(oMyElement);
        } catch(Exception e){
        }   
        // Return the hashmap
        return hsmResponse;
    }
    // Get the Main Link of the CPU
    private String getMainLink(DomElement oDomElement){
        String strLink = new String("");
        // Get all <a></a> tags
        DomNodeList<HtmlElement> oDomElementsA = oDomElement.getElementsByTagName("a");
        // Loop over all tags getted
        for(int i = 0; i<oDomElementsA.size(); i++){            
            // If has the "Link" text
            if(oDomElementsA.get(i).asText().trim().contains("Link")){
                // Save the URL
                strLink = oDomElementsA.get(i).getAttribute("href");
            }
        }    
        // Return the link
        return strLink;
    }
    @Override
    protected DomElement getMyElement(DomElement oDomElement, String strKeyWord){
       // Get all Titles Section
        ArrayList<DomElement> arrlTitlesElement = this.getDataElementByClassFromNode("systemRequirementsTitle", oDomElement, true);
        DomElement oAuxElement = null;
        DomElement oMyElement = null;
        // Loop over all titles elements
        for(Iterator<DomElement> item = arrlTitlesElement.iterator(); item.hasNext(); ){
            oAuxElement = item.next();
            // If one of all has my key word
            if(oAuxElement.asText().trim().contains(strKeyWord)){                
                // Get the parent element of my parent element :D
                oMyElement = (DomElement) oAuxElement.getParentNode().getParentNode();
            }
        }       
        // Return that elements
        return oMyElement;
    }
    @Override
    protected HashMap getHashMapFromTableElement(DomElement oDomElement) {
        // Get the Titles and Values's Wrapper
        ArrayList<DomElement> oTitlesElements = this.getDataElementByClassFromNode("specificationsWiderTitleBox", oDomElement, false),
                              oValuesElements = this.getDataElementByClassFromNode("systemRequirementsSmallerBox", oDomElement, false);
        // Create a new HashMap
        HashMap hsmResponse = new HashMap();
        // Iterator
        int i = 0;
        // Loop over the Titles - could be with the values array, whatever, it's the same.
        for(i = 0; i<oTitlesElements.size(); i++){
            // Put into the hashmap the title(key) and the value(value)
            hsmResponse.put(this.processInfoTitle(oTitlesElements.get(i)), this.processInfoValue(oValuesElements.get(i)));
        }
        // Return HashMap
        return hsmResponse;
    }   
}