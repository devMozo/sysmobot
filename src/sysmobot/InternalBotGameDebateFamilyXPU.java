/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysmobot;

import com.gargoylesoftware.htmlunit.html.DomElement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author SysMo
 */
public abstract class InternalBotGameDebateFamilyXPU extends Bot{
    
    public InternalBotGameDebateFamilyXPU(String strRootPage) {
        super(strRootPage);
        // I only want the HTML's text!!!
        this._oWebClient.getOptions().setCssEnabled(false);
        this._oWebClient.getOptions().setDownloadImages(false);
        this._oWebClient.getOptions().setJavaScriptEnabled(false);    
        this._oWebClient.getOptions().setThrowExceptionOnScriptError(false);
        this._oWebClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
    }  

    @Override
    protected ArrayList<HashMap> getDataAsADictionary() {
        // Get the main Element that has ID!!!!! Incredible!
        DomElement mainElement = this.getDataElementById("gambody", true).iterator().next();
        // Array that will contain all family's gpus
        ArrayList<DomElement> arrlDomElements = this.getDataElementByClassFromNode("hardwareRow", mainElement, true);
        // Dictionary
        ArrayList<HashMap> arrlDictionary = new ArrayList<HashMap>();
        // Loop over all GPUS Elements
        for(Iterator<DomElement> item = arrlDomElements.iterator(); item.hasNext();){
            arrlDictionary.add(this.extractMainData(item.next()));
        }
        // Return dictionary
        return arrlDictionary;
    }

    @Override
    protected HashMap extractMainData(DomElement oDomElement) {    
        // Create a new HashMap
        HashMap hmpDictionaryPage = new HashMap();
        // Set all keys and values that I need       
        hmpDictionaryPage.put("Name", this.getNameXPU(oDomElement));    
        hmpDictionaryPage.put("URL", this.getURLXPU(oDomElement));
        hmpDictionaryPage.put("ID", this.getIDXPU(oDomElement));
        hmpDictionaryPage.put("Performance", this.getPerformanceXPU((String)hmpDictionaryPage.get("URL")));
        
        
        /*
        TEST CODE
        HashMap asd = (HashMap) hmpDictionaryPage.get("PerformanceGPU");
        
        Map<String, Object> map = asd;
        
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            System.out.println("Key = " + entry.getKey());
            
            Map<String, String> map2 = (Map<String, String>) entry.getValue();
            
            for (Map.Entry<String, String> entry2 : map2.entrySet()) {
                System.out.println(entry2.getKey() + " = " + entry2.getValue());
            }
        }*/
        
        // Return this dictionary page        
        return hmpDictionaryPage;     
    }
    // Get XPU's Name
    private String getNameXPU(DomElement oDomElement){
        // I nkow that there is only one class with that name
        // but the getDataElementByClassFromNode returns me an array
        String strName = new String("");
        try{
            // Get the first element that contains the GPU Name
            DomElement oGPUNameClassElement = this.getDataElementByClassFromNode("hardwareDeriv", oDomElement, false).iterator().next(); 
            // And set the element as a text
            strName = oGPUNameClassElement.asText();
            // This is a fucking text, it doesn't matter
            strName = strName.replace("[not released]", "");
        } catch(Exception e){
            
        }     
        return strName;
    }
    // Get XPU's URL
    protected String getURLXPU(DomElement oDomElement){
        // I nkow that there is only one class with that name
        // but the getDataElementByClassFromNode returns me an array
        String strURL = new String("");
        try{
            // Get the first element that contains the GPU's url
            DomElement oURLGPUClassElement = this.getDataElementByClassFromNode("hardwareDeriv", oDomElement, false).iterator().next(); 
            // And set the element as a text
            strURL = oURLGPUClassElement.getElementsByTagName("a").iterator().next().getAttribute("href");
            strURL = strURL.replace("..", "");
        } catch(Exception e){
            
        }     
        return strURL;        
    }    
    // Get XPU's ID
    protected abstract String getIDXPU(DomElement oDomElement); 
    // Get Perfomance XPU
    protected abstract HashMap getPerformanceXPU(String strURL);
}