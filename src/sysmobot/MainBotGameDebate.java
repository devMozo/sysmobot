/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysmobot;

import com.gargoylesoftware.htmlunit.html.DomElement;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author SysMo
 */
public class MainBotGameDebate extends Bot {

    public MainBotGameDebate(String strRootPage) {
        super(strRootPage);
        // I only want the HTML's text!!!
        this._oWebClient.getOptions().setCssEnabled(false);
        this._oWebClient.getOptions().setDownloadImages(false);
        this._oWebClient.getOptions().setJavaScriptEnabled(false);    
        this._oWebClient.getOptions().setThrowExceptionOnScriptError(false);
        this._oWebClient.getOptions().setThrowExceptionOnFailingStatusCode(false);         
    }  
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, InterruptedException{              
        // Set the root page
        Bot bGame = new MainBotGameDebate("http://www." + URLEncoder.encode("game-debate", "UTF-8") + ".com");
        // Gets the current date and time
        Calendar calNow = Calendar.getInstance();
        // The current year
        int iYear = calNow.get(Calendar.YEAR);       
        // Create an array list that will contains all games
        ArrayList<HashMap> arrlGames = new ArrayList<HashMap>();
        // Loop form the 2000 to the current year.
        // The year 2000 is because the Game-Debate has game from that date
        for(int i = 2000; i <= iYear; i++){
            bGame.setSectionPage("/games/index.php?year="+i);
            // Save all games from this year in the game's array
            arrlGames.addAll(bGame.getDataAsADictionary());
        }
        
        System.out.println(arrlGames);
    }   

    @Override
    protected ArrayList<HashMap> getDataAsADictionary() {
        // Get all class
        ArrayList<DomElement> oDomElementSrRowFull = this.getDataElementByClass("srRowFull", false);
        // Data, my precious Data!!
        ArrayList<HashMap> arrlDataAsDictionary = new ArrayList<HashMap>();
        // Remove all PS3, Wii and XBOX360 Games, because I don't need it
        oDomElementSrRowFull = this.removeXBOX360Games(oDomElementSrRowFull);
        oDomElementSrRowFull = this.removePS3Games(oDomElementSrRowFull);
        oDomElementSrRowFull = this.removeWiiGames(oDomElementSrRowFull);
        // Use of PoolThread, for any reasy, please contact this page: http://tutorials.jenkov.com/java-util-concurrent/executorservice.html
        // Creating a pool of 5 threads
        ExecutorService exsExecutor = Executors.newScheduledThreadPool(8);  
        // Complete the HashMap Array
        for( Iterator<DomElement> item = oDomElementSrRowFull.parallelStream().iterator(); item.hasNext(); ){
            // This variable will contain any result that call() function return
            Future oFuture = exsExecutor.submit(new Callable(){
                public Object call() throws Exception {
                    System.out.println(Thread.currentThread().getName()+" (Start) ");  
                    return extractMainData(item.next());
                }
            });
            // Try to take the HashMap and add to the dictionary
            try {
                arrlDataAsDictionary.add((HashMap) oFuture.get());
            } catch (InterruptedException ex) {
                Logger.getLogger(MainBotGameDebate.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(MainBotGameDebate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        // Leave to receive task
        exsExecutor.shutdown();  
        // Untill the execution has terminated
        while (!exsExecutor.isTerminated()) {   }  
        // And finally return
        return arrlDataAsDictionary;        
    }   

    @Override
    protected HashMap extractMainData(DomElement oDomElement) {
        // Create a new HashMap
        HashMap hmpDictionaryPage = new HashMap();
        // Set all keys and values that I need
        hmpDictionaryPage.put("GameName", this.getNameFromDomElement(oDomElement));
        hmpDictionaryPage.put("URL", this.getURLFromDomElement(oDomElement));
        hmpDictionaryPage.put("Date", this.getDateFromDomElement(oDomElement));
        hmpDictionaryPage.put("Genre", this.getGenreFromDomElement(oDomElement));
        hmpDictionaryPage.put("Requirements", this.getInternalDataFromInternalBot((String)hmpDictionaryPage.get("URL")));
        // Return this dictionary page        
        return hmpDictionaryPage;        
    } 
    // Remove all games that are for XBOX 360 consoles
    private ArrayList<DomElement> removeXBOX360Games(ArrayList<DomElement> arrlDomElement){
        // A new DomElement
        DomElement oActualElement = null;
        // Iterate the array
        for( Iterator<DomElement> item = arrlDomElement.iterator(); item.hasNext(); ){
            oActualElement = item.next();
            // If has the class "xbox360Link"
            if(this.getDataElementByClassFromNode("xbox360Link", oActualElement, false).iterator().hasNext()){
               // Remove from the array
               item.remove();
            }
        }
        // Return the sanitize array
        return arrlDomElement;
    }
    // Remove all games that are for PS3 consoles
    private ArrayList<DomElement> removePS3Games(ArrayList<DomElement> arrlDomElement){
        // A new DomElement
        DomElement oActualElement = null;
        // Iterate the array
        for( Iterator<DomElement> item = arrlDomElement.iterator(); item.hasNext(); ){
            oActualElement = item.next();
            // If has the class "xbox360Link"
            if(this.getDataElementByClassFromNode("ps3Link", oActualElement, false).iterator().hasNext()){
               // Remove from the array
               item.remove();
            }
        }
        // Return the sanitize array
        return arrlDomElement;
    }
    // Remove all games that are for PS3 consoles
    private ArrayList<DomElement> removeWiiGames(ArrayList<DomElement> arrlDomElement){
        // A new DomElement
        DomElement oActualElement = null;
        // Iterate the array
        for( Iterator<DomElement> item = arrlDomElement.iterator(); item.hasNext(); ){
            oActualElement = item.next();
            // If has the class "xbox360Link"
            if(this.getDataElementByClassFromNode("wiiLink", oActualElement, false).iterator().hasNext()){
               // Remove from the array
               item.remove();
            }
        }
        // Return the sanitize array
        return arrlDomElement;
    }
    // Get Name From the DOM element
    private String getNameFromDomElement(DomElement oDomElement){
        // I nkow that there is only one class with that name
        // but the getDataElementByClassFromNode returns me an array
        String strName = new String("");
        try{
            DomElement oTitleFullClassElement = this.getDataElementByClassFromNode("srTitleFull", oDomElement, false).iterator().next(); 
            strName = oTitleFullClassElement.getElementsByTagName("a").iterator().next().asText();
        } catch(Exception e){
            
        }
        // I find in the DOM element the tag "<a></a>" that contains the Game's Name      
        return strName;
    }
    // Get URL from the DOM element
    private String getURLFromDomElement(DomElement oDomElement){        
        // I nkow that there is only one class with that name
        // but the getDataElementByClassFromNode returns me an array
        String strURL = new String("");
        try{
            DomElement oURLFullClassElement = this.getDataElementByClassFromNode("srTitleFull", oDomElement, false).iterator().next();  
            // This returns me something like "../games/index.php?g_id=24239&game=Accounting"
            strURL = oURLFullClassElement.getElementsByTagName("a").iterator().next().getAttribute("href");
            strURL = strURL.replace("..", "");            
        } catch(Exception e){
            
        }        
        // And I need just "/games/index.php?g_id=24239&game=Accounting", so I need to parse the string
        return strURL;
    }
    // Get Date from the DOM element
    private String getDateFromDomElement(DomElement oDomElement){
        // I nkow that there is only one class with that name
        // but the getDataElementByClassFromNode returns me an array
        String strDate = new String("");
        // Try to get the Daate String
        try{
            DomElement oDateFullClassElement = this.getDataElementByClassFromNode("srDateFull", oDomElement, false).iterator().next();
            strDate = oDateFullClassElement.asText();
        } catch(Exception e){
            
        }
        // Return Date
        return strDate;
    }
    // Get the genre from the DOM element
    private String getGenreFromDomElement(DomElement oDomElement){
        // I nkow that there is only one class with that name
        // but the getDataElementByClassFromNode returns me an array
        String strGenre = new String("");
        // Try to get the Daate String
        try{
            DomElement oGenreFullClassElement = this.getDataElementByClassFromNode("srGenreFull", oDomElement, false).iterator().next();
            strGenre = oGenreFullClassElement.asText();
        } catch(Exception e){
            
        }
        return strGenre;
    }    
    private HashMap getInternalDataFromInternalBot(String strSectionPage){
        
        InternalBotGameDebate oInternalBot = new InternalBotGameDebate(this._strRootPage);
        HashMap hsmFields = null;
        
        try{
            oInternalBot.setSectionPage(strSectionPage);
            hsmFields = oInternalBot.getDataAsADictionary().iterator().next();
        } catch( Exception e ){
            hsmFields = new HashMap();
        }  
        
        return hsmFields;   
    }
}