/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysmobot;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author user
 */
public class MainBotGameDebateCPU extends MainBotGameDebateXPU {
    
    public MainBotGameDebateCPU(String strRootPage) {
        super(strRootPage);
    }
    
    public static void main(String[] args) throws IOException, InterruptedException{              
        // Set the root page
        Bot bCPU = new MainBotGameDebateCPU("http://www." + URLEncoder.encode("game-debate", "UTF-8") + ".com");    
        // Create an array list that will contains all GPU
        ArrayList<HashMap> arrlCPU = new ArrayList<HashMap>();
        ArrayList<String> arrlSection = new ArrayList<String>();
                          arrlSection.add("cpuDesktop");
                          arrlSection.add("cpuLaptop");
        // Loop for all GPU's Sections
        for(int i = 0; i <= arrlSection.size(); i++){
            bCPU.setSectionPage("/hardware/index.php?list="+arrlSection.get(i));
            // Save all GPU from this section in the GPU's array
            arrlCPU.addAll(bCPU.getDataAsADictionary());
        }
        
        System.out.println(arrlCPU);
    }
    
    @Override
    protected ArrayList<HashMap> getDataAsADictionary() {
        // New ArrayList of HashMap that we'll return after
        ArrayList<HashMap> arrlHashMapsResponse = new ArrayList<HashMap>();
        // Array with all families
        ArrayList<HashMap> arrlHashMapsFamilies = super.getDataAsADictionary(); 
        // Array that will contain all data extracted from the InternalBot
        ArrayList<HashMap> arrlExtractedFromInternalBot = null;        
        // Loop over all families
        for(HashMap hsmItem : arrlHashMapsFamilies){
            // Create a new Internal Bot
            Bot oInternalGPUBot = new InternalBotGameDebateFamilyCPU(getStrRootPage());
            try {
                System.out.println(Thread.currentThread().getName()+" (Start) "); 
                // Try to set the new page
                oInternalGPUBot.setSectionPage((String)hsmItem.get("FamilyURL"));
            } catch (IOException ex) {
                Logger.getLogger(MainBotGameDebateXPU.class.getName()).log(Level.SEVERE, null, ex);
            }   
            // Get all data from the InternalBot
            arrlExtractedFromInternalBot = oInternalGPUBot.getDataAsADictionary();
            // Add the Owner and Family's Values
            for(HashMap hsmInternalItem : arrlExtractedFromInternalBot){
                hsmInternalItem.putAll(hsmItem);
            }
            // Add to the response array            
            arrlHashMapsResponse.addAll(oInternalGPUBot.getDataAsADictionary());                        
        }         
        // Return the dictionary :D
        return arrlHashMapsResponse;       
    }     
}
