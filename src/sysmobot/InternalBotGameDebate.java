/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysmobot;

import com.gargoylesoftware.htmlunit.html.DomElement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author SysMo
 */
public class InternalBotGameDebate extends Bot{

    private DomElement _oWrapperSysRequirement;
    // Set also the System Requirement's Wrapper
    public InternalBotGameDebate(String strRootPage) {
        super(strRootPage);  
        // I only want the HTML's text!!!
        this._oWebClient.getOptions().setCssEnabled(false);
        this._oWebClient.getOptions().setDownloadImages(false);
        this._oWebClient.getOptions().setJavaScriptEnabled(false);    
        this._oWebClient.getOptions().setThrowExceptionOnScriptError(false);
        this._oWebClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
    }

    @Override
    public void setSectionPage(String strSection) throws IOException {
        super.setSectionPage(strSection); 
        this.setWrapperSystemRequirement();
    }
         
    @Override
    protected ArrayList<HashMap> getDataAsADictionary() {
        ArrayList<HashMap> arrlDictionary = null;
        
        if(this._oWrapperSysRequirement != null){
            arrlDictionary = new ArrayList<HashMap>();
            arrlDictionary.add(this.extractMainData(_oWrapperSysRequirement));
        }
        
        return arrlDictionary;
    }  
    
    @Override
    protected HashMap extractMainData(DomElement oDomElement) {
        // Create a new HashMap
        HashMap hmpDictionaryPage = new HashMap();
        // Set all keys and values that I need
        hmpDictionaryPage.put("Minimum", this.getMinimumRequirements());
        hmpDictionaryPage.put("Recommended", this.getRecommendedRequirements());
        // Return this dictionary page        
        return hmpDictionaryPage;   
    } 
    // Set the wrapper class
    private void setWrapperSystemRequirement(){
        try{
            this._oWrapperSysRequirement = this.getDataElementByClass("systemReqWrapper", false).iterator().next();
        }
        catch(Exception e){
            this._oWrapperSysRequirement = null;
        }
    }
    // Get the minimun requirements
    private HashMap getMinimumRequirements(){
        // Create a new hashmap
        HashMap hsmMinRequirements = new HashMap();
        // Save all Minimum Requirements
        hsmMinRequirements.put("IntelCPU", this.getIntelCPUMin());
        hsmMinRequirements.put("AMDCPU", this.getAMDCPUMin());
        hsmMinRequirements.put("GraphicCardNvidia", this.getGraphicCardMinNvidia());
        hsmMinRequirements.put("GraphicCardNvidiaID", this.getGraphicCardMinNvidiaID());
        hsmMinRequirements.put("GraphicCardAMD", this.getGraphicCardMinAMD());        
        hsmMinRequirements.put("GraphicCardAMDID", this.getGraphicCardMinAMDID());
        hsmMinRequirements.put("VRAM", this.getVRAMMin());
        hsmMinRequirements.put("RAM", this.getRAMMin());
        hsmMinRequirements.put("OS", this.getOSMin());
        hsmMinRequirements.put("Disk", this.getDiskMin());        
        // Return
        return hsmMinRequirements;
    }
    // Get the recommended requirements
    private HashMap getRecommendedRequirements(){
        // Create a new hashmap
        HashMap hsmRecommenderRequirements = new HashMap();
        // Save all Recommended Requirements
        hsmRecommenderRequirements.put("IntelCPU", this.getIntelCPURecommended());
        hsmRecommenderRequirements.put("AMDCPU", this.getAMDCPURecommended());
        hsmRecommenderRequirements.put("GraphicCardNvidia", this.getGraphicCardNvidiaRecommended());
        hsmRecommenderRequirements.put("GraphicCardNvidiaID", this.getGraphicCardNvidiaRecommendedID());
        hsmRecommenderRequirements.put("GraphicCardAMD", this.getGraphicCardAMDRecommended());
        hsmRecommenderRequirements.put("GraphicCardAMDID", this.getGraphicCardAMDRecommendedID());
        hsmRecommenderRequirements.put("VRAM", this.getVRAMRecommended());
        hsmRecommenderRequirements.put("RAM", this.getRAMRecommended());
        hsmRecommenderRequirements.put("OS", this.getOSRecommended());
        hsmRecommenderRequirements.put("Disk", this.getDiskRecommended());        
        // Returnn
        return hsmRecommenderRequirements;
    }
    // Get the Inctel CPU Min
    private String getIntelCPUMin(){
        // Initialize String
        String strCPU = new String("");
        // First try to get the wrapper CPUS
        // And then get the wrapper Intel's CPU
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxCPUMin", this._oWrapperSysRequirement, false).iterator().next();
                       oDomElement = this.getDataElementByClassFromNode("systemRequirementsLinkSubTop", oDomElement, false).iterator().next();
            // Assign Intel Name to String
            strCPU = oDomElement.asText();
        } catch( Exception e ){}
        // Returnn
        return strCPU;
    }
    // Get the AMD CPU Min
    private String getAMDCPUMin(){
        // Initialize String
        String strCPU = new String("");
        // First try to get the wrapper CPUS
        // And then get the wrapper AMD's CPU        
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxCPUMin", this._oWrapperSysRequirement, false).iterator().next();
                       oDomElement = this.getDataElementByClassFromNode("systemRequirementsLinkSubBtm", oDomElement, false).iterator().next();
            // Assign AMD Name to String
            strCPU = oDomElement.asText();
        } catch( Exception e ){}
        // RETURNNN
        return strCPU;
    }
    // Get the minimum grafic card Nvdia
    private String getGraphicCardMinNvidia(){
        // Initialize String
        String strGraphicCard = new String("");
        // First try to get the wrapper GPU
        // And then get the wrapper Nvidia's GPU        
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxGPUMin", this._oWrapperSysRequirement, false).iterator().next();
                       oDomElement = this.getDataElementByClassFromNode("systemRequirementsLinkSubTop", oDomElement, false).iterator().next();
            // Assign Nvidia Name to String
            strGraphicCard = oDomElement.asText();
        } catch( Exception e ){}
        // RETURNNN
        return strGraphicCard;
    }
    // Get the ID of the GraphicCard
    private String getGraphicCardMinNvidiaID(){
        // Initialize String
        String strGraphicCard = new String("");
        // First try to get the wrapper GPU
        // And then get the wrapper Nvidia's GPU        
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxGPUMin", this._oWrapperSysRequirement, false).iterator().next();
                       oDomElement = this.getDataElementByClassFromNode("systemRequirementsLinkSubTop", oDomElement, false).iterator().next();
            // Assign Nvidia ID to String
            strGraphicCard = oDomElement.getElementsByTagName("a").iterator().next().getAttribute("href");
            // Get the ID From the URL
            strGraphicCard = GameDebateParser.parseAndReturnIDGraphicCard(strGraphicCard);
        } catch( Exception e ){}
        // RETURNNN
        return strGraphicCard;       
    }    
    // Get the minimum grafic card AMD
    private String getGraphicCardMinAMD(){
        // Initialize String
        String strGraphicCard = new String("");
        // First try to get the wrapper GPU
        // And then get the wrapper AMD's GPU        
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxGPUMin", this._oWrapperSysRequirement, false).iterator().next();
                       oDomElement = this.getDataElementByClassFromNode("systemRequirementsLinkSubBtm", oDomElement, false).iterator().next();
            // Assign AMD Name to String
            strGraphicCard = oDomElement.asText();
        } catch( Exception e ){}
        // RETURNNN
        return strGraphicCard;
    }
    // Get the Grafic Card AMD's ID
    private String getGraphicCardMinAMDID(){
        // Initialize String
        String strGraphicCard = new String("");
        // First try to get the wrapper GPU
        // And then get the wrapper AMD's GPU        
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxGPUMin", this._oWrapperSysRequirement, false).iterator().next();
                       oDomElement = this.getDataElementByClassFromNode("systemRequirementsLinkSubBtm", oDomElement, false).iterator().next();
            // Assign AMD ID to String
            strGraphicCard = oDomElement.getElementsByTagName("a").iterator().next().getAttribute("href");
            // Get the ID From the URL
            strGraphicCard = GameDebateParser.parseAndReturnIDGraphicCard(strGraphicCard);
        } catch( Exception e ){}
        // RETURNNN
        return strGraphicCard;
    }
    // Get the minimun VRAM
    private String getVRAMMin(){
        // Initialize String
        String strVRAM = new String("");
        // Try to get the VRAM element       
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxVRAMMin", this._oWrapperSysRequirement, false).iterator().next();
            // Assign VRAM Name to String
            strVRAM = oDomElement.asText();
        } catch( Exception e ){}
        // RETURNNN
        return strVRAM;
    }
    // Get the minimun Ram
    private String getRAMMin(){
        // Initialize String
        String strRAM = new String("");
        // Try to get the RAM element       
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxRAMMin", this._oWrapperSysRequirement, false).iterator().next();
            // Assign RAM Name to String
            strRAM = oDomElement.asText();
        } catch( Exception e ){}
        // RETURNNN
        return strRAM;
    }
    // Get the minimun Operating System 
    private String getOSMin(){
        // Initialize String
        String strOS = new String("");
        // Try to get the OS element       
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxSystemMin", this._oWrapperSysRequirement, false).iterator().next();
            // Assign OS Name to String
            strOS = oDomElement.asText();
        } catch( Exception e ){}
        // RETURNNN
        return strOS;
    }
    // Get minimun disk available
    private String getDiskMin(){
        // Initialize String
        String strDisk = new String("");
        // First I need to get the DirectX Element from the HTML
        // This is bacause the "game-debate.com" is a shit, and it's programmed by monkeys
        // The I get the slibling DIV and there is my data :D
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxDirectXMin", this._oWrapperSysRequirement, false).iterator().next();
                       oDomElement = oDomElement.getNextElementSibling();
            // Assign Disk Name to String
            strDisk = oDomElement.asText();
        } catch( Exception e ){}
        // RETURNNN
        return strDisk;
    }
    // Get the recommended CPU Intel
    private String getIntelCPURecommended(){        
        // Initialize String
        String strCPU = new String("");
        // First try to get the wrapper CPUS
        // And then get the wrapper Intel's CPU
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxCPU", this._oWrapperSysRequirement, true).iterator().next();
                       oDomElement = this.getDataElementByClassFromNode("systemRequirementsLinkSubTop", oDomElement, true).iterator().next();
            // Assign Intel Name to String
            strCPU = oDomElement.asText(); 
        } catch( Exception e ){}
        // Returnn
        return strCPU;
    }
    // Get the recommended CPU AMD
    private String getAMDCPURecommended(){
        // Initialize String
        String strCPU = new String("");
        // First try to get the wrapper CPUS
        // And then get the wrapper AMD's CPU        
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxCPU", this._oWrapperSysRequirement, true).iterator().next();
                       oDomElement = this.getDataElementByClassFromNode("systemRequirementsLinkSubBtm", oDomElement, true).iterator().next();
            // Assign AMD Name to String
            strCPU = oDomElement.asText();
        } catch( Exception e ){}
        // RETURNNN
        return strCPU;
    }
    // Get recommended NVDIA
    private String getGraphicCardNvidiaRecommended(){
        // Initialize String
        String strGraphicCard = new String("");
        // First try to get the wrapper GPU
        // And then get the wrapper Nvidia's GPU        
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxGPU", this._oWrapperSysRequirement, true).iterator().next();
                       oDomElement = this.getDataElementByClassFromNode("systemRequirementsLinkSubTop", oDomElement, true).iterator().next();
            // Assign Nvidia Name to String
            strGraphicCard = oDomElement.asText();
        } catch( Exception e ){}
        // RETURNNN
        return strGraphicCard;
    }
    // Get recommended NVDIA's ID
    private String getGraphicCardNvidiaRecommendedID(){
        // Initialize String
        String strGraphicCard = new String("");
        // First try to get the wrapper GPU
        // And then get the wrapper Nvidia's GPU        
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxGPU", this._oWrapperSysRequirement, true).iterator().next();
                       oDomElement = this.getDataElementByClassFromNode("systemRequirementsLinkSubTop", oDomElement, true).iterator().next();
            // Assign Nvidia Name to String
            strGraphicCard = oDomElement.getElementsByTagName("a").iterator().next().getAttribute("href");
            // Get the ID From the URL
            strGraphicCard = GameDebateParser.parseAndReturnIDGraphicCard(strGraphicCard);
        } catch( Exception e ){}
        // RETURNNN
        return strGraphicCard;
    }
    // Get recommended Card AMD
    private String getGraphicCardAMDRecommended(){
        // Initialize String
        String strGraphicCard = new String("");
        // First try to get the wrapper GPU
        // And then get the wrapper AMD's GPU        
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxGPU", this._oWrapperSysRequirement, true).iterator().next();
                       oDomElement = this.getDataElementByClassFromNode("systemRequirementsLinkSubBtm", oDomElement, true).iterator().next();
            // Assign AMD Name to String
            strGraphicCard = oDomElement.asText();
        } catch( Exception e ){}
        // RETURNNN
        return strGraphicCard;
    }
     // Get Recommended Card AMD's ID
    private String getGraphicCardAMDRecommendedID(){
        // Initialize String
        String strGraphicCard = new String("");
        // First try to get the wrapper GPU
        // And then get the wrapper AMD's GPU        
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxGPU", this._oWrapperSysRequirement, true).iterator().next();
                       oDomElement = this.getDataElementByClassFromNode("systemRequirementsLinkSubBtm", oDomElement, true).iterator().next();
            // Assign AMD Name to String
            strGraphicCard = oDomElement.getElementsByTagName("a").iterator().next().getAttribute("href");
            // Get the ID From the URL
            strGraphicCard = GameDebateParser.parseAndReturnIDGraphicCard(strGraphicCard);
        } catch( Exception e ){}
        // RETURNNN
        return strGraphicCard;
    }
    // Recommended VRAM
    private String getVRAMRecommended(){
        // Initialize String
        String strVRAM = new String("");
        // Try to get the VRAM element       
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxVRAM", this._oWrapperSysRequirement, true).iterator().next();
            // Assign VRAM Name to String
            strVRAM = oDomElement.asText();
        } catch( Exception e ){}
        // RETURNNN
        return strVRAM;
    }
    // Get recommended RAM
    private String getRAMRecommended(){
        // Initialize String
        String strRAM = new String("");
        // Try to get the RAM element       
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxRAM", this._oWrapperSysRequirement, true).iterator().next();
            // Assign RAM Name to String
            strRAM = oDomElement.asText();
        } catch( Exception e ){}
        // RETURNNN
        return strRAM;
    }
    // Get recommended OS
    private String getOSRecommended(){
        // Initialize String
        String strOS = new String("");
        // Try to get the OS element       
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxSystem", this._oWrapperSysRequirement, true).iterator().next();
            // Assign OS Name to String
            strOS = oDomElement.asText();
        } catch( Exception e ){}
        // RETURNNN
        return strOS;
    }
    // Get recommended DISK
    private String getDiskRecommended(){
        // Initialize String
        String strDisk = new String("");
        // First I need to get the DirectX Element from the HTML
        // This is bacause the "game-debate.com" is a shit, and it's programmed by monkeys
        // The I get the slibling DIV and there is my data :D
        try{
            DomElement oDomElement = this.getDataElementByClassFromNode("systemRequirementsHwBoxDirectX", this._oWrapperSysRequirement, true).iterator().next();
                       oDomElement = oDomElement.getNextElementSibling();
            // Assign Disk Name to String
            strDisk = oDomElement.asText();
        } catch( Exception e ){}
        // RETURNNN
        return strDisk;
    }
    
}