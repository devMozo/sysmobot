/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysmobot;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.CookieManager;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author SysMo
 */
public abstract class Bot{
    
    protected WebClient _oWebClient;
    protected HtmlPage _oVictimWeb;
    protected ArrayList<String> _arrlKeys;
    protected String _strRootPage;
    
    // Constructor for our Robot, set also the root of the page
    // For example, if the page is "https://facebook.com/something_else"
    // You must to save in the root "https://facebook.com" (without the last slash)
    public Bot(String strRootPage){
        // My web client :D
        this._oWebClient = new WebClient(BrowserVersion.CHROME); 
        this._strRootPage = strRootPage;
        // Disabled Cookies
        CookieManager cookieManager = new CookieManager();
                      cookieManager.setCookiesEnabled(false);
        this._oWebClient.setCookieManager(cookieManager);
    }    
    // Set a new section to the WebClient
    // With the last example would be "/something_else",
    // The first slash is required but the last slash is not necessary
    public void setSectionPage(String strSection) throws IOException{
        System.out.println("Loading: " + this._strRootPage + strSection);
        this._oVictimWeb = this._oWebClient.getPage(this._strRootPage + strSection);
        System.out.println(this._oVictimWeb.getWebResponse().getStatusCode());
    }    
    // Get the HTML from the current page
    public String getHTML(){
        WebResponse oResponse = this._oVictimWeb.getWebResponse();
        return oResponse.getContentAsString();
    }    
    // Get the URL Root
    public String getStrRootPage() {
        return _strRootPage;
    }    
    // Get the DataElement passing a condition, it's a combination of an Attribute(class,id,name,etc) and the String to compare 
    private ArrayList<DomElement> getDataElementByCondition(String oAttribute, String strToCompare, DomNode nodeElement, boolean bGetExactly) { 
        // Array that will contain all data 
        ArrayList<DomElement> arrlDomElements = new ArrayList<DomElement>(); 
        // Aux DomElement to save in the loop each element
        DomElement oAuxDomElement = null;   
        // Loop each element       
        for(Iterator<DomElement> itrElement = nodeElement.getDomElementDescendants().iterator(); itrElement.hasNext();){
            // Get the element
            oAuxDomElement = itrElement.next();            
            // If I want the exactly name of the attribute
            if(bGetExactly){
                // Parse the attr's value by " "(white-space)
                String[] arrStringDataToCompare = strToCompare.split(" ");
                String[] arrStringValues = oAuxDomElement.getAttribute(oAttribute).split(" ");
                // Loop the string array
                for(String item : arrStringValues){
                    // Also I compare each compare String
                    for(String itemToCompare : arrStringDataToCompare){
                        // If the value is completly the same to the strToCompare
                        if(item.equals(itemToCompare)){
                            // Add to the array
                            arrlDomElements.add(oAuxDomElement);
                        }
                    }
                }
            }
            else{
                // If it has the class "srRowFull" for example, I'll save it in the array 
                if(oAuxDomElement.getAttribute(oAttribute).contains(strToCompare)){
                    // Add to the array
                    arrlDomElements.add(oAuxDomElement);
                }
            }
        } 
        // Return the array
        return arrlDomElements;       
    }
    // UNIVERSAL GET
    // Get Data Element By Clasname
    protected ArrayList<DomElement> getDataElementByClass(String strClasName, boolean bGetExactly) {
        return this.getDataElementByCondition("class", strClasName, this._oVictimWeb, bGetExactly);
    }
    // Get Data Element By ID
    protected ArrayList<DomElement> getDataElementById(String strId, boolean bGetExactly){
        return this.getDataElementByCondition("id", strId, this._oVictimWeb, bGetExactly);
    }
    // Get Data Element By an Attribute
    protected ArrayList<DomElement> getDataElementByAttribute(String strAttribute, String strTextAttribute, boolean bGetExactly){
        return this.getDataElementByCondition(strAttribute, strTextAttribute, this._oVictimWeb, bGetExactly);
    }  
    // FROM SPECIFIC NODE
    // Get Data Element By Clasname  From specific Node
    protected ArrayList<DomElement> getDataElementByClassFromNode(String strClasName, DomNode nodeElement, boolean bGetExactly) {
        return this.getDataElementByCondition("class", strClasName, nodeElement, bGetExactly);
    }
    // Get Data Element By ID  From specific Node
    protected ArrayList<DomElement> getDataElementByIdFromNode(String strId, DomNode nodeElement, boolean bGetExactly){
        return this.getDataElementByCondition("id", strId, nodeElement, bGetExactly);
    }
    // Get Data Element By an Attribute From specific Node
    protected ArrayList<DomElement> getDataElementByAttributeFromNode(String strAttribute, String strTextAttribute, DomNode nodeElement, boolean bGetExactly){
        return this.getDataElementByCondition(strAttribute, strTextAttribute, nodeElement, bGetExactly);
    }  
    // Get the data needed as a Dictionary(Array of HashMaps) from the current page
    protected abstract ArrayList<HashMap> getDataAsADictionary(); 
    // Extract the main fields from a DOM Element, and then it'll returned in a HashMap
    protected abstract HashMap extractMainData(DomElement oDomElement);    
}