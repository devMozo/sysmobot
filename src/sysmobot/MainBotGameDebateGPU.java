/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysmobot;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author user
 */
public class MainBotGameDebateGPU extends MainBotGameDebateXPU{
    
    public MainBotGameDebateGPU(String strRootPage) {
        super(strRootPage);
    }
    
    public static void main(String[] args) throws IOException, InterruptedException{              
        // Set the root page
        Bot bGPU = new MainBotGameDebateGPU("http://www." + URLEncoder.encode("game-debate", "UTF-8") + ".com");    
        // Create an array list that will contains all GPU
        ArrayList<HashMap> arrlGPU = new ArrayList<HashMap>();
        ArrayList<String> arrlSection = new ArrayList<String>();
                          arrlSection.add("gfxDesktop");
                          arrlSection.add("gfxLaptop");
        // Loop for all GPU's Sections
        for(int i = 0; i <= arrlSection.size(); i++){
            bGPU.setSectionPage("/hardware/index.php?list="+arrlSection.get(i));
            // Save all GPU from this section in the GPU's array
            arrlGPU.addAll(bGPU.getDataAsADictionary());
        }
        
        System.out.println(arrlGPU);
    }   
    
    @Override
    protected ArrayList<HashMap> getDataAsADictionary() {
        // New ArrayList of HashMap that we'll return after
        ArrayList<HashMap> arrlHashMapsResponse = new ArrayList<HashMap>();
        // Array with all families
        ArrayList<HashMap> arrlHashMapsFamilies = super.getDataAsADictionary();  
        // Array that will contain all data extracted from the InternalBot
        ArrayList<HashMap> arrlExtractedFromInternalBot = null;
        // Loop over all families
        for(HashMap hsmItem : arrlHashMapsFamilies){
            // Create a new Internal Bot
            Bot oInternalGPUBot = new InternalBotGameDebateFamilyGPU(getStrRootPage());
            MainBotDevicesPCI oBotDevices = null;
            try {
                System.out.println(Thread.currentThread().getName()+" (Start) "); 
                // Try to set the new page
                oInternalGPUBot.setSectionPage((String)hsmItem.get("FamilyURL"));
            } catch (IOException ex) {
                Logger.getLogger(MainBotGameDebateXPU.class.getName()).log(Level.SEVERE, null, ex);
            }    
            // Get all data from the InternalBot
            arrlExtractedFromInternalBot = oInternalGPUBot.getDataAsADictionary();
            // Add the Owner and Family's Values
            for(HashMap hsmInternalItem : arrlExtractedFromInternalBot){
                oBotDevices = (MainBotDevicesPCI)hsmItem.get("BotPCI");
                // If the bot is null the PCI-ID too
                if(oBotDevices == null){
                    hsmItem.put("PCI-ID", "");
                } 
                else{
                    // Get the GPU's Name
                    String strNameGPU = (String)hsmInternalItem.get("Name");
                    // Get the GPU's Arquitecture
                    // Why? Because I need to make an String that allow me to compare each words of this GPU 
                    // with the words that are allocated in the PCI-Devices website
                    // To get the arquitecture I need to get first the Performance(HashMap), after that I get the Features(Hashmap)
                    // and finally get the arquitecture :D
                    HashMap hsmPerformanceGPU = (HashMap)hsmInternalItem.get("Performance"),
                            hsmFeaturesGPU = (HashMap)hsmPerformanceGPU.get("GPUFeatures");
                    String  strArquitecture = (String)hsmFeaturesGPU.get("Architecture");
                            
                    oBotDevices.setStrGameDebateDevices(strNameGPU + " " + strArquitecture);
                    hsmItem.put("PCI-ID", oBotDevices.getStrOwnerHexa()+"-"+oBotDevices.getDataAsString());
                    System.out.println(strNameGPU +" - "+strArquitecture+" - "+oBotDevices.getDataAsString());
                }             
                // Put all items in this dictionary's page
                hsmInternalItem.putAll(hsmItem);
            }
            // Add to the response array
            arrlHashMapsResponse.addAll(arrlExtractedFromInternalBot);                        
        }         
        // Return the dictionary :D
        return arrlHashMapsResponse;       
    }    
}
