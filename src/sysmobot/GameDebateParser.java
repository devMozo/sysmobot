/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sysmobot;

/**
 *
 * @author SysMo
 */
public class GameDebateParser {
    
    // Get the ID Graphic Card
    public static String parseAndReturnIDGraphicCard(String strGraphicCard){
        // Verify if exist the Graphic Card's ID in the URL
        if(strGraphicCard.contains("gid=")){
            // Get the position of the beginning of the ID
            Integer iPosIDBeginning = strGraphicCard.indexOf("gid=");
            // Get the substring starting from the position that we have getted before
            strGraphicCard = strGraphicCard.substring(iPosIDBeginning);
            // Try to parse and get the ID
            try{
                strGraphicCard = strGraphicCard.split("&")[0];
                strGraphicCard = strGraphicCard.split("=")[1];
            } catch(Exception e){
                strGraphicCard = "";
            }
        }
        return strGraphicCard;
    }
    // Get the ID Processor
    public static String parseAndReturnIDProcessor(String strProcessor) {
        // Verify if exist the Graphic Card's ID in the URL
        if(strProcessor.contains("pid=")){
            // Get the position of the beginning of the ID
            Integer iPosIDBeginning = strProcessor.indexOf("pid=");
            // Get the substring starting from the position that we have getted before
            strProcessor = strProcessor.substring(iPosIDBeginning);
            // Try to parse and get the ID
            try{
                strProcessor = strProcessor.split("&")[0];
                strProcessor = strProcessor.split("=")[1];
            } catch(Exception e){
                strProcessor = "";
            }
        }
        return strProcessor;
    }
}
